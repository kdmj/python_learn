#Link List Node Creating class
class Node:
	def __init__(self, dataval=None):
		self.dataval = dataval
		self.nextval = None
#End of LLN Class

#Slingly Link List Class
class SinglyLinkList ():
	def  __init__(self):
		self.headval = None
					
	def printLinkList(self):		#Print the SLL
		printval = self.headval
		while printval is not None:
			print (printval.dataval)
			printval = printval.nextval
#End of SLL Class

#Primary No identifying function

def IsPrimeNo(InputVal):
	SqrtInputVal = int(InputVal**0.5 +1)
	NotPrimeFlag = 0
	D = 2
	primaryLoopCount = 1 
	while (D <= SqrtInputVal):
		if (InputVal%D == 0):
			NotPrimeFlag = 1
		D = D + 1
        	
	if (NotPrimeFlag == 0):
		return 1
	else :
		return 0
#End of function




#Creating Link List and main function

print ("Start")


GreatNo = 600851475143                   #data we need to calculate
SQGreatNo = int(GreatNo**.5 +1)          #square root of "Great no"

SSL = SinglyLinkList()
SSL.headval = Node(2)
PriviousNode = Node (3)
SSL.headval.nextval = PriviousNode
NodeCount = 2
PrimeFlag = 0
for x in range (5,SQGreatNo):			#SQGreatNo
	if(IsPrimeNo(x) == 1):
		NewNode = Node(x)
		PriviousNode.nextval = NewNode
		PriviousNode = NewNode
		if (GreatNo%x==0):
			print (x," is prime factor of ",GreatNo)

		
print ("end")

#SSL.printLinkList()

#End of link list work

