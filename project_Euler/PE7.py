def isPrimeNo(inputNo):
    sqInputNo = inputNo**0.5+1
    D = 2
    primeNoFlag = 0

    while (D <= sqInputNo):
        if (inputNo%D == 0):
            primeNoFlag = 1
        D +=1

    if (primeNoFlag == 0):
        return 1
    else:
        return 0

primeNoCount = 1
inputNo = 3

while (primeNoCount<=10000):
    if (isPrimeNo(inputNo) == 1):
        primeNoCount +=1
        print (primeNoCount , " - ", inputNo)
        
    inputNo +=1


