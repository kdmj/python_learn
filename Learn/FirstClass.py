print ("Test 16")

class Employee:
	
	EmployeeCount = 0

	def __init__(self, name, salary):
		self.name = name
		self.salary = salary
		Employee.EmployeeCount +=1 
	
	def displayEmployeeCount(self):
		print ("Total no of Employee ", Employee.EmployeeCount)
	
	def displayEmployee(self):
		print("Employee Name : " ,self.name,  "\t Employee Salary : " , self.salary)
#pass


Emp1 = Employee("Mark",85000)
Emp2 = Employee("Malith",90000)
print(Employee.EmployeeCount)
print (Emp1.name)
print (Emp2.salary)
Emp1.displayEmployee()
Emp2.displayEmployee()
print ("End")

