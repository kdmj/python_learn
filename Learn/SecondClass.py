class Animal:

	AnimalCount = 0
	AnimalLocation = "Earth"
	
	def __init__(self, AnimalType, Name, Age):
		self.AnimalType = AnimalType
		self.Name = Name
		self.Age = Age
		Animal.AnimalCount = Animal.AnimalCount + 1
	
	def DisplayAnimal(self):
		print(self.AnimalType, self.Name, self.Age)
		
		
Cat = Animal("Cat", "Meaw", 2)
		
Dog = Animal("Dog", "Blackey", 8)

Cat.DisplayAnimal()
Dog.DisplayAnimal()

print("")
print (Animal.AnimalCount)
print (Dog.AnimalType)
print (Animal.AnimalLocation)
